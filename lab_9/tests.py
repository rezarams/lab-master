from django.test import Client
from django.urls import resolve, reverse
from django.test import TestCase
from .views import index
from .csui_helper import get_client_id, get_access_token, get_data_user,verify_user
from .api_enterkomputer import get_drones
from .custom_auth import auth_login, auth_logout
import json
import environ
import requests
root = environ.Path(__file__) - 3
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.

API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"

class LabUnitTest(TestCase):
    def test_lab_9_url_name_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)
    
    def test_api_enterkomputer(self):
        drones = get_drones()
        self.assertNotEqual(drones, None)

    def test_get_client_id(self):
        self.assertEqual(get_client_id(), "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")
    
    def test_profile_not_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_profile_login(self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_reset_favorite_drone_keyError(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})

        response_post = self.client.post(reverse('lab-9:clear_session_drones'))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil reset favorite drones', html_response)


    def test_add_delete_and_reset_favorite_drone(self):
        #login
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        
        #test menambah
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894}))
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107893}))
        response_post = self.client.post(reverse('lab-9:profile'))
        
        #test menghapus
        response_post = self.client.post(reverse('lab-9:del_session_drones', kwargs={'id':107894}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil hapus dari favorite',html_response)
        
        #test mereset favorit drones
        response_post = self.client.post(reverse('lab-9:clear_session_drones'))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil reset favorite drones',html_response)

        #logout
        response_post = self.client.post(reverse('lab-9:auth_logout'))


    def test_login_cookie_page(self):
        #login session
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        # test template yang digunakan pada halaman login cookie
        response_post = self.client.get(reverse('lab-9:cookie_login'))
        self.assertTemplateUsed(response_post, 'lab_9/cookie/login.html')
        #test jika method yang digunakan pada cookie_auth_login bukan post
        response_post = self.client.get(reverse('lab-9:cookie_auth_login'))
        self.assertEqual(response_post.status_code, 302)
        #test jika halaman profile cookie diakses tanpa login
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response_post.status_code, 302)
        #test jika username dan password salah
        response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': 'xx', 'password': 'xxx'})
        response_post = self.client.get(reverse('lab-9:cookie_login'))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Username atau Password Salah',html_response)
        #test login pada halaman cookie dengan data yang valid
        response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': 'utest', 'password': 'ptest'})
        response_post = self.client.get(reverse('lab-9:cookie_login'))
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertTemplateUsed(response_post, 'lab_9/cookie/profile.html')
        #test jika cookie diset secara manual (usaha hacking)
        response = self.client.get(reverse('lab-9:cookie_profile'))
        response.client.cookies['user_login'] = 'xxsdadax'
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertTemplateUsed(response_post, 'lab_9/cookie/login.html')
        #test logout halaman cookie
        response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': 'utest', 'password': 'ptest'})
        response_post = self.client.get(reverse('lab-9:cookie_clear'))
        self.assertEqual(response_post.status_code, 302)

    # Test api_enterkomputer.py
    def test_drones_api(self):
        response = requests.get('https://www.enterkomputer.com/api/product/drone.json')
        self.assertEqual(response.json(),get_drones().json())

    def test_verify_function(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_VERIFY_USER, params=parameters)
        result = verify_user(access_token)
        self.assertEqual(result,response.json())
        
    def test_get_data_user_function(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}

        npm = json.loads(requests.get(API_VERIFY_USER, params=parameters).content)["identity_number"]
        response = requests.get(API_MAHASISWA+npm, params=parameters)

        result = get_data_user(access_token,npm)
        self.assertEqual(result,response.json())


    # Test custom_auth
    def test_login_auth(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        self.assertEqual(response_post.status_code, 302)

    def test_fail_login(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': 'aku', 'password': 'ganteng'})
        response = self.client.get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn('Username atau password salah',html_response)

    def test_logout_auth(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.post(reverse('lab-9:auth_logout'))
        response = self.client.get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

