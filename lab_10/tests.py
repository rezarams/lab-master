from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token
from .views import index
from .models import Pengguna, MovieKu
import environ

# Create your tests here.
root = environ.Path(__file__) - 3
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab10UnitTest(TestCase):
    def setUp(self):
        self.username = env('SSO_USERNAME')
        self.password = env('SSO_PASSWORD')
        self.movieId = 'tt5013056'

    def test_lab_10_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_root_url_use_index_funct(self):
        response = resolve('/lab-10/')
        self.assertEqual(response.func, index)

    def test_lab_10_login_success(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('/lab_10/dashboard.html')

    def test_lab_10_login_fail(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':"", 'password': ""})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/lab-10/',302,200)

    def test_access_dashboard_without_login(self):
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_access_dashboard_login(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Pengguna.objects.all().count(),1)

    def test_movie_list(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-10/movie/list/')
        self.assertEqual(response.status_code, 200)

    def test_movie_detail(self):
        response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
        self.assertEqual(response.status_code, 200)

    def test_add_watch_later_logged_in(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/detail/'+self.movieId+'/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)

    def test_add_watch_later_without_logged_in(self):
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)

    def test_list_watch_later(self):
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)
	
    def test_add_watched_with_login(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)

    def test_add_watched_without_login(self):
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)

    def test_list_watched(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieId+'/')
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-10/custom_auth/logout/')
        html_response = self.client.get('/lab-10/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)

    def test_api(self):
        response = self.client.get('/lab-10/api/movie/Inception/2010/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)

    def test_pengguna_model(self):
        Pengguna.objects.create(kode_identitas='1606879905',nama="Nicholas")
        self.assertEqual(Pengguna.objects.all().count(),1)

    def test_movieku_model(self):
        pengguna = 	Pengguna.objects.create(kode_identitas='1606879905',nama="Nicholas")
        MovieKu.objects.create(pengguna=pengguna, kode_movie = self.movieId)
        self.assertEqual(MovieKu.objects.all().count(), 1)
