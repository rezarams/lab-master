//Chatbox
$('textarea#enter').keypress(function(event) {
    if(event.keyCode == 13 && !event.shiftKey) {
	    event.preventDefault();
	    var inputTxt = "Anon: " + $('textarea#enter').val() +"<br>";            // Create element with HTML  
		
		$("div.msg-insert").append(inputTxt);
		document.getElementById("enter").value = "";
	  }
});



//Change Theme
//inisiasi data tema
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	
  var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

  //untuk apply default theme, dijalankan saat web pertama kali dibuka
 if (localStorage.getItem("selectedTheme") === null) {
   localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
 }
  //untuk simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));

  
  
  //untuk memuat tema ke select2
  var retrievedObject = themes;
  $('.my-select').select2({'data': retrievedObject});

  console.log(retrievedObject);
  
  //untuk menerapkan selectedTheme yang ada di local storage
  var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
  var key;
  var bcgColor;
  var fontColor;
  for (key in retrievedSelected) {
  	if (retrievedSelected.hasOwnProperty(key)) {
       	bcgColor=retrievedSelected[key].bcgColor;
       	fontColor=retrievedSelected[key].fontColor;
   	}
  }  
  $("body").css({"background-color": bcgColor});
  $("footer").css({"color":fontColor});
	
	
	//implementasi fungsi tombol apply
	$('.apply-button').on('click', function(){  // sesuaikan class button
    var valueTheme = $('.my-select').val();
    var theme;
    var a;
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});



// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	var print = document.getElementById('print');
  if (x === 'ac') {
    /* implemetnasi clear all */
	print.value = "";
	erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
};
// END