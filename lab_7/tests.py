from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list
from .api_csui_helper.csui_helper import CSUIhelper
import json
from .models import Friend


def initiate_friend():
	page = 2
	response = Client().post('/lab-7/change-page/', {'page':page})
	listOfMhs = json.loads(json.loads(response.content.decode('utf-8'))['listOfMhs'])
	friend = listOfMhs[0]
	return friend

# Create your tests here.
class Lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		response = resolve('/lab-7/')
		self.assertEqual(response.func, index)

	def test_lab_7_friend_list_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_friend_list_using_friend_list_func(self):
		response = resolve('/lab-7/friend-list/')
		self.assertEqual(response.func, friend_list)
	
	def test_models_return_as_dict(self):
		name = 'Chanek'
		npm = '1010101010'
		Friend.objects.create(friend_name=name, npm=npm)
		friend_obj = Friend.objects.all()[0]
		friend_dict = friend_obj.as_dict()
		self.assertEqual(name, friend_dict['friend_name'])
		self.assertEqual(npm, friend_dict['npm'])

	def test_lab_7_can_delete_object(self):
		Friend.objects.create(friend_name='Chanek', npm='1010101010')
		friend_obj = Friend.objects.all()[0]
		response = Client().post('/lab-7/delete-friend/' + str(friend_obj.id) +'/', {'id':friend_obj.id})
		self.assertEqual(Friend.objects.all().count(), 0)

	def test_lab_7_can_validate_existing_npm(self):
		Friend.objects.create(friend_name='Chanek', npm='1010101010')
		friend_obj = Friend.objects.all()[0]
		response = Client().post('/lab-7/validate-npm/', {'npm':friend_obj.npm})
		respDict = json.loads(response.content.decode('utf-8'))
		self.assertTrue(respDict['is_taken'])

	# def test_lab_7_can_validate_unpresent_npm_in_database(self):
		# response = Client().post('/lab-7/validate-npm/', {'npm':'1001'})
		# respDict = json.loads(response.content.decode('utf-8'))
		# self.assertFalse(respDict['is_taken'])

	def test_model_can_create_new_friend(self):
		friend = initiate_friend()
		Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
		self.assertEqual(Friend.objects.all().count(), 1)

	def test_csui_helper_with_wrong_password(self):
		try:
			csui_helper = CSUIhelper('anonim', 'test')
		except Exception as e:
			self.assertIn('username atau password sso salah', str(e))
		
	def test_model_can_create_from_addfriend(self):
		nama = 'Chanek'
		npm = npm = '1010101010'
		response = Client().post('/lab-7/add-friend/', {'name': nama, 'npm':npm})
		self.assertEqual(Friend.objects.all().count(), 1)
		responseFriendList = json.loads(Client().get('/lab-7/get-friend-list/').content.decode('utf-8'))
		self.assertEqual(responseFriendList['results'][0]['friend_name'], nama)

	def test_model_cannot_create_existing_friend_from_addfriend(self):
		friend = initiate_friend()
		Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
		response = Client().post('/lab-7/add-friend/', {'name':friend['nama'], 'npm':friend['npm']})
		self.assertEqual(Friend.objects.all().count(), 1)